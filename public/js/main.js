import {foo} from './jquery-3.4.0.min_.js';
import {boo} from './bootstrap.min_.js';
import {getEngData, commingSectionTextEng} from './sections_data_eng.js';
import {getRusData, commingSectionTextRus} from './sections_data_rus.js';
foo()
boo()
$(function () {
  var link_handler = function (e) {
    window.open(e.data.link, "_blank")
  }

  // Данные из которых формируются карточки,
  // порадок данных важен для отображения данных.
  // :param title:
  //        Заголовок карточки
  // :param text:
  //        Контентная часть карточки
  // :param link:
  //        По клику на карточку переход будет по указанной ссылке, если ссылка пустая, то событие не назначается
  // :param icon:
  //        Название файла иконки в каталоге img, если строка пустая, то иконка не добавляется
  // :param en:
  //        Является ли пункт ссылкой на англоязычную документацию
  if(lang == "eng"){
    var data = getEngData;
    var commingSectionText = commingSectionTextEng;
  }
  else{
    var data = getRusData;
    var commingSectionText = commingSectionTextRus;
  }
  var c_cont = $("#accordion");
  for (var i = 0; i < data.length; i++) {
    var doc = data[i];
    let d_col = $('<div>', {
      'id': 'heading' + i,
      'class': 'col-md-12 d-flex align-items-stretch pb-3 collapsed',
      'data-toggle': 'collapse',
      'data-target': '#collapse' + i,
      'aria-expanded': 'true',
      'aria-controls': 'collapse' + i
    });
    let d_card = $('<div>', { 'class': 'card w-100' });
    let d_card_body = $('<div>', { 'class': 'card-body' });
    let d_card_h = $('<h2>', { 'class': 'card-title d-inline-block' });
    let d_card_img = $("<img>", { class: "js_arrow_top", src: '/img/arrow-top.svg' });

    d_card_h.text(doc.title)
    d_card_h.appendTo(d_card_body)
    d_card_body.appendTo(d_card)
    d_card_img.appendTo(d_card_body);
    d_card.appendTo(d_col)
    d_col.appendTo(c_cont)

    let d_coll_div = $('<div>', {
      'id': 'collapse' + i,
      'class': 'collapse',
      'aria-labelledby': 'heading' + i,
      'data-parent': '#accordion'
    });
    d_coll_div.appendTo(c_cont)

    var itms_cont = $('#collapse' + i);
    var itms = doc.items;
    for (var j = 0; j < itms.length; j++) {
      var comming = " (" + commingSectionText + ")"
      var card_info = itms[j];
      var cursor_class = ""
      if(card_info.enabled != "true"){
        card_info.title += comming
        card_info.icon = 'lock@3x.png'
        cursor_class = "block-cursor"
      }
      let b_col = $('<div>', {
        'class': 'col-lg-4 col-md-6 col-xs-6 d-flex align-items-stretch pb-3 pt-3'
      });
      let b_card = $('<div>', { 'class': 'card-light w-100 ' + cursor_class });
      let b_card_body = $('<div>', { 'class': 'card-body' });
      let b_card_info = $('<div>', { class: 'card-info' });
      let b_card_h = $('<h2>', { 'class': 'card-light-title d-inline-block '});

      b_card_h.text(card_info.title)
      b_card_h.appendTo(b_card_info);
      //b_card_h.appendTo(b_card_body)
      b_card_info.appendTo(b_card_body);
      b_card_body.appendTo(b_card)
      b_card.appendTo(b_col)

      if (card_info.icon.length) {
        var image_path = ""
        if(lang != "rus" & lang != ""){
          image_path += "../"
        }
        image_path += 'img/' + card_info.icon
        var card_icon = $('<img>', { 'class': 'd-inline-block float-right', 'height': '40px', 'width': '40px', 'src': image_path })
        card_icon.appendTo(b_card_body)
      }

      var card_text = $('<p>', { 'class': 'card-light-text' }).text(card_info.text)
      card_text.appendTo(b_card_info);
      //card_text.appendTo(b_card_body)

      b_col.appendTo(itms_cont)

      if (card_info.link && card_info.enabled == "true") {
        b_card.click({ link: doc.link + '/' + card_info.link }, link_handler)
      }
    }
  }
});
