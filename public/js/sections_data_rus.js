import {icons} from './icons.js';
export const getRusData = [
    {
      'title': 'Весы с печатью этикеток и прикассовые серии M-ER 725, 727, 828 на базе ОС Android и Linux',
      'link': 'scales',
      'items': [
        {
          title: "Руководство администратора",
          text: "Настройка и эксплуатация",
          link: "admin",
          icon: icons.doc,
          enabled: "true",
        },
        {
          title: "Руководство программиста",
          text: "Описание протокола весов",
          link: "protocol",
          icon: icons.code,
          enabled: "true",
        },
        {
          title: "Драйвер весов",
          text: "Описание команд драйвера",
          link: "driver",
          icon: icons.doc,
          enabled: "true",
        },
        {
          title: "Программа «Менеджер весов»",
          text: "Инструкция по работе с ПО «Менеджер весов»",
          link: "manager",
          icon: icons.doc,
          enabled: "true",
        },
        {
          title: "Программа «Менеджер задач»",
          text: "Инструкция по работе с ПО «Менеджер задач»",
          link: "task_manager",
          icon: icons.doc,
          enabled: "true",
        },
        {
          title: "Руководство интегратора",
          text: "Примеры подключения «Драйвера весов» к товароучетному ПО",
          link: "integration",
          icon: icons.doc,
          enabled: "true",
        },
        {
          title: "Версии ПО",
          text: "Раздел с актуальными версиями ПО \n" +
              "для скачивания",
          link: "software",
          icon: icons.file,
          enabled: "true",
        },
      ]
    }, 
    {
      'title': 'Терминал оплаты СБП',
      'link': 'sbp',
      'items': [
        {
          'title': 'Программа «СБП Терминал» Windows/Linux',
          'text': 'Инструкция по работе с ПО «СБП Терминал»',
          'link': 'manager',
          'icon': icons.doc,
          'enabled': 'true',
        },
        {
          'title': 'Мобильное приложение "СБП терминал MERTECH"',
          'text': 'Инструкция по работе с ПО "СБП терминал MERTECH"',
          'link': 'manager_android',
          'icon': icons.doc,
          'enabled': 'true',
        },
        {
          'title': 'Протокол',
          'text': 'Описание протокола драйвера',
          'link': 'protocol',
          'icon': icons.doc,
          'enabled': 'true',
        },
        {
          'title': 'Версии ПО',
          'text': 'Раздел с актуальными версиями ПО для скачивания',
          'link': 'software',
          'icon': icons.doc,
          'enabled': 'true',
        },
      ]
    }, 
    {
      'title': 'Интерфейсные весы серии M-ER 221, 222, 224, 333, 326, 328, 828',
      'link': 'scales_pos',
      'items': [
        {
          'title': 'Протокол',
          'text': 'Описание протокола весов',
          'link': 'protocol',
          'icon': icons.doc,
          'enabled': 'true',
        },
        {
          'title': 'Менеджер ',
          'text': 'Настройка и эксплуатация',
          'link': 'manager',
          'icon': icons.doc,
          'enabled': 'true',
        },
        {
          'title': 'Драйвер',
          'text': 'Описание команд драйвера',
          'link': 'driver',
          'icon': icons.doc,
          'enabled': 'true',
        },
        {
          'title': 'Руководство интегратора',
          'text': 'Примеры подключения «Драйвера весов» к товароучетному ПО',
          'link': 'integration',
          'icon': icons.doc,
          'enabled': 'true',
        },
        {
          'title': 'Версии ПО',
          'text': 'Раздел с актуальными версиями ПО для скачивания',
          'link': 'software',
          'icon': icons.doc,
          'enabled': 'true',
        },
      ]
    }, 
    {
      'title': 'Терминалы сбора данных',
      'link': 'tsd',
      'items': [
        {
          'title': '1С БПО для сканеров ТСД MERTECH в режиме broadcast',
          'text': 'Настройка и эксплуатация',
          'link': '1c_bpo',
          'icon': icons.doc,
          'enabled': 'false',
        },
        {
          'title': 'MERTECH Seuic Q9,Q9C (SDK)',
          'text': 'Руководство программиста',
          'link': 'q9',
          'icon': icons.doc,
          'enabled': 'false',
        },
        {
          'title': 'MERTECH MovFast (SDK)',
          'text': 'Руководство программиста',
          'link': 'movfast',
          'icon': icons.doc,
          'enabled': 'true',
        },
        {
          'title': 'Версии ПО',
          'text': 'Раздел с актуальными версиями ПО для скачивания',
          'link': 'software',
          'icon': icons.doc,
          'enabled': 'false',
        },
      ]
    },
    {
      'title': 'Модуль видеораспознавания товаров VISION-AI',
      'link': 'vision_ai',
      'items': [
        {
          'title': 'Руководство программиста',
          'text': 'Описание REST API к модулю VISION-AI',
          'link': 'rest_api',
          'icon': icons.doc,
          'enabled': 'true',
        },
        {
          'title': 'Менеджер',
          'text': 'Настройка и эксплуатация',
          'link': 'manager',
          'icon': icons.doc,
          'enabled': 'true',
        },
        {
          'title': 'Версии ПО',
          'text': 'Раздел с актуальными версиями ПО для скачивания',
          'link': 'software',
          'icon': icons.doc,
          'enabled': 'true',
        },
      ]
    },
    {
      'title': 'Прикассовые весы с разпознаванием',
      'link': 'linux_scale',
      'items': [
        {
          'title': 'Руководство программиста',
          'text': 'Описание протокола весов',
          'link': 'protocol',
          'icon': icons.doc,
          'enabled': 'true',
        },
      ]
    },
    {
      'title': 'Мобильное приложение 1С для весов M-ER 725, 728',
      'link': '1c_mobile_app_scale_m_er',
      'items': [
        {
          'title': 'Руководство администратора',
          'text': 'Описание, установка и настройка',
          'link': 'admin',
          'icon': icons.doc,
          'enabled': 'true',
        },
      ]
    },    
  ];
  export const commingSectionTextRus = "В процессе разработки"