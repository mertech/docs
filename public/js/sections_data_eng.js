import {icons} from './icons.js';
export const getEngData = [
    {
      'title': 'Scales for checkout and label printing M-ER Series 725, 727, 828 based on Android and Linux OS',
      'link': 'scales',
      'items': [
        {
          'title': 'Administrator\'s Guide',
          'text': 'Configuration and Operation',
          'link': 'admin',
          'icon': icons.doc,
          'enabled': 'true',
      },
      {
          'title': 'Programmer\'s Guide',
          'text': 'Description of the Scale Protocol',
          'link': 'protocol',
          'icon': icons.doc,
          'enabled': 'true',
      },
      {
          'title': 'Scale Driver',
          'text': 'Description of Driver Commands',
          'link': 'driver',
          'icon': icons.doc,
          'enabled': 'true',
      },
      {
          'title': 'Scale Manager Program',
          'text': 'Instructions for Using the "Scale Manager" Software',
          'link': 'manager',
          'icon': icons.doc,
          'enabled': 'true',
      },
      {
          'title': 'Task Manager Program',
          'text': 'Instructions for Using the "Task Manager" Software',
          'link': 'task_manager',
          'icon': icons.doc,
          'enabled': 'true',
      },
      {
          'title': 'Integrator\'s Guide',
          'text': 'Examples of Connecting the "Scale Driver" to Inventory Software',
          'link': 'integration',
          'icon': icons.doc,
          'enabled': 'false',
      },
      {
          'title': 'Software',
          'text': 'Section with current software versions for download',
          'link': 'software',
          'icon': icons.doc,
          'enabled': 'false',
      }      
      ]
    }, 
    {
      'title': 'SBP Payment Terminal',
      'link': 'sbp',
      'items': [
        {
          'title': 'SBP Terminal Program for Windows/Linux',
          'text': 'Instructions for Using the "SBP Terminal" Software',
          'link': 'manager',
          'icon': icons.doc,
          'enabled': 'false',
        },
        {
          'title': 'SBP Terminal Program for Android',
          'text': 'Instructions for Using the "SBP Terminal" Software',
          'link': 'manager_android',
          'icon': icons.doc,
          'enabled': 'false',
        },
        {
          'title': 'Windows/Linux Driver',
          'text': 'Description of Driver Commands',
          'link': 'driver',
          'icon': icons.doc,
          'enabled': 'false',
        },
        {
          'title': 'Protocol',
          'text': 'Description of the Driver Protocol',
          'link': 'protocol',
          'icon': icons.doc,
          'enabled': 'false',
        },
        {
          'title': 'Software',
          'text': 'Section with current software versions for download',
          'link': 'software',
          'icon': icons.doc,
          'enabled': 'false',
        }
      ]
    },    
    {
      'title': 'Interface Scales M-ER Series 221, 222, 224, 333, 326, 328, 828',
      'link': 'scales_pos',
      'items': [
        {
          'title': 'Protocol',
          'text': 'Description of the Scale Protocol',
          'link': 'protocol',
          'icon': icons.doc,
          'enabled': 'false',
        },
        {
          'title': 'Manager',
          'text': 'Configuration and Operation',
          'link': 'manager',
          'icon': icons.doc,
          'enabled': 'false',
        },
        {
          'title': 'Driver',
          'text': 'Description of Driver Commands',
          'link': 'driver',
          'icon': icons.doc,
          'enabled': 'false',
        },
        {
          'title': 'Integrator\'s Guide',
          'text': 'Examples of Connecting the "Scale Driver" to Inventory Software',
          'link': 'integration',
          'icon': icons.doc,
          'enabled': 'false',
        },
        {
          'title': 'Software',
          'text': 'Section with current software versions for download',
          'link': 'software',
          'icon': icons.doc,
          'enabled': 'true',
        }
      ]
    },    
    {
      'title': 'Data Collection Terminals',
      'link': 'tsd',
      'items': [
        {
          'title': '1C Barcode Scanner TSD MERTECH',
          'text': 'Configuration and Operation',
          'link': '1c_bpo',
          'icon': icons.doc,
          'enabled': 'false',
        },
        {
          'title': 'MERTECH Seuic Q9, Q9C (SDK)',
          'text': 'Programmer\'s Guide',
          'link': 'q9',
          'icon': icons.doc,
          'enabled': 'false',
        },
        {
          'title': 'MERTECH MovFast (SDK)',
          'text': 'Programmer\'s Guide',
          'link': 'movfast',
          'icon': icons.doc,
          'enabled': 'true',
        },
        {
          'title': 'Software',
          'text': 'Section with current software versions for download',
          'link': 'software',
          'icon': icons.doc,
          'enabled': 'false',
        },
      ]
   },
   {
    'title': 'VISION-AI Product Recognition Module',
    'link': 'vision_ai',
    'items': [
      {
        'title': 'Programmer\'s Guide',
        'text': 'Description of the REST API for the VISION-AI Module',
        'link': 'rest_api',
        'icon': icons.doc,
        'enabled': 'true',
      },
      {
        'title': 'Manager',
        'text': 'Configuration and Operation',
        'link': 'manager',
        'icon': icons.doc,
        'enabled': 'false',
      },
      {
        'title': 'Software',
        'text': 'Section with current software versions for download',
        'link': 'software',
        'icon': icons.doc,
        'enabled': 'true',
      },
    ]
 },
 {
  'title': 'Checkout Scales with Recognition',
  'link': 'linux_scale',
  'items': [
    {
      'title': 'Programmer\'s Guide',
      'text': 'Description of the Scale Protocol',
      'link': 'protocol',
      'icon': icons.doc,
      'enabled': 'false',
    },
  ]
},
];
export const commingSectionTextEng = "Under Construction"